class Openssh < Formula
  desc "OpenBSD freely-licensed SSH connectivity tools"
  homepage "http://www.openssh.com/"
  version "7.3p1"
  url "http://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-#{version}.tar.gz"
  sha256 "3ffb989a6dcaa69594c3b550d4855a5a2e1718ccdde7f5e36387b424220fbecc"
  revision 1

  option "with-hpn", "Add HPN patch"
  option "with-keychain-support", "Add native OS X Keychain and Launch Daemon support to ssh-agent"
  option "with-libressl", "Build with LibreSSL instead of OpenSSL"
  option "with-test", "Run openssh's tests"

  depends_on "autoconf" => :build if build.with?("keychain-support")
  depends_on "openssl" => :recommended
  depends_on "libressl" => :optional
  depends_on "ldns" => :optional
  depends_on "pkg-config" => :build if build.with? "ldns"

  patch do
    url "https://bitbucket.org/api/2.0/snippets/dwfreed/GEygd/a39c1b40bef3e81b03e3c46bb98085e42096a472/files/0001-PAM.patch"
    sha256 "4d533c842c3dab7edcecd1b5c603e4ef93a89b3a3d17e1b70007d16c28c7fd41"
  end

  patch do
    url "https://bitbucket.org/api/2.0/snippets/dwfreed/GEygd/a39c1b40bef3e81b03e3c46bb98085e42096a472/files/0002-Sandbox.patch"
    sha256 "25925c93214f1b9d4a91c28824837bf018aae9454d55599dfc56a9a322b31dca"
  end

  patch do
    url "https://bitbucket.org/api/2.0/snippets/dwfreed/GEygd/a39c1b40bef3e81b03e3c46bb98085e42096a472/files/0003-launchd.patch"
    sha256 "13cf1877124c554555c65dfdaa41e6e07dc423ddb358ae2dcd5c9a234390eb21"
  end

  if build.with? "keychain-support"
    patch do
      url "https://bitbucket.org/api/2.0/snippets/dwfreed/GEygd/a39c1b40bef3e81b03e3c46bb98085e42096a472/files/0004-Apple-keychain-integration-other-changes.patch"
      sha256 "4e25a2f893bb11c1a9e58e223debfb2254fb5e23ffc1ed95bf757a7490fd5d53"
    end
  end

  if build.with? "hpn"
    patch do
      url "https://bitbucket.org/api/2.0/snippets/dwfreed/GEygd/6fe9a51734e3ad5935a7aaf9d1143ceb0962d254/files/0005-HPN.patch"
      sha256 "906a0228d0c9806541bca043c32518b70a4874c811a49d9e2ec609a7dd145003"
    end
  end

  def install
    system "autoreconf", "-i" if build.with?("keychain-support")

    ENV.append "CPPFLAGS", "-D__APPLE_SANDBOX_NAMED_EXTERNAL__"

    args = %W[
      --with-libedit
      --with-pam
      --with-kerberos5
      --prefix=#{prefix}
      --sysconfdir=#{etc}/ssh
      --with-privsep-user=_sshd
    ]

    if build.with? "libressl"
      args << "--with-ssl-dir=#{Formula["libressl"].opt_prefix}"
    else
      args << "--with-ssl-dir=#{Formula["openssl"].opt_prefix}"
    end

    if build.with? "keychain-support"
      args << "--with-keychain=apple"
      ENV.append "CPPFLAGS", "-D__APPLE_LAUNCHD__ -D__APPLE_MEMBERSHIP__ -D__APPLE_XSAN__"
    end

    args << "--with-ldns" if build.with? "ldns"

    system "./configure", *args
    system "make"
    system "make", "install"
    system "make", "-j1", "interop-tests", "compat-tests", "tests" if build.with? "test"
  end

  def caveats
    if build.with? "keychain-support" then <<-EOS.undent
        NOTE: replacing system daemons is unsupported. Proceed at your own risk.

        For complete functionality, please modify:
          /System/Library/LaunchAgents/org.openbsd.ssh-agent.plist

        and change ProgramArguments from
          /usr/bin/ssh-agent
        to
          #{HOMEBREW_PREFIX}/bin/ssh-agent

        You will need to restart or issue the following commands
        for the changes to take effect:

          launchctl unload /System/Library/LaunchAgents/org.openbsd.ssh-agent.plist
          launchctl load /System/Library/LaunchAgents/org.openbsd.ssh-agent.plist

        After that, you can start storing private key passwords in
        your OS X Keychain.
      EOS
    end
  end

  test do
    system bin/"ssh", "-V"
  end
end
