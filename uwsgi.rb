class Uwsgi < Formula
  desc "Full stack for building hosting services"
  homepage "https://uwsgi-docs.readthedocs.org/en/latest/"
  url "https://projects.unbit.it/downloads/uwsgi-2.0.13.1.tar.gz"
  sha256 "2eca0c2f12ab76f032154cd147f4d5957d3195a022678d59cb507f4995a48d7f"

  head "https://github.com/unbit/uwsgi.git"

  depends_on "pkg-config" => :build
  depends_on "pcre"
  depends_on "openssl"
  depends_on "sqlite"
  depends_on "yajl"
  depends_on :python if MacOS.version <= :snow_leopard

  depends_on "python" => :optional
  depends_on "python3" => :optional

  def install
    # "no such file or directory: '... libpython2.7.a'"
    # Reported 23 Jun 2016: https://github.com/unbit/uwsgi/issues/1299
    ENV.delete("SDKROOT")

    ENV.append %w[CFLAGS LDFLAGS], "-arch #{MacOS.preferred_arch}"

    (buildpath/"buildconf/brew.ini").write <<-EOS.undent
      [uwsgi]
      ssl = true
      json = yajl
      yaml = embedded
      inherit = base
      plugin_dir = #{libexec}/uwsgi
      embedded_plugins = null
    EOS

    system "python", "uwsgiconfig.py", "--verbose", "--build", "brew"

    plugins = %w[
      airbrake
      alarm_curl
      alarm_speech
      asyncio
      cache
      carbon
      cgi
      cheaper_backlog2
      cheaper_busyness
      corerouter
      cplusplus
      curl_cron
      dumbloop
      dummy
      echo
      emperor_amqp
      fastrouter
      forkptyrouter
      gevent
      http
      ldap
      logcrypto
      logfile
      logpipe
      logsocket
      msgpack
      notfound
      pam
      ping
      psgi
      pty
      rawrouter
      router_basicauth
      router_cache
      router_expires
      router_hash
      router_http
      router_memcached
      router_metrics
      router_radius
      router_redirect
      router_redis
      router_rewrite
      router_static
      router_uwsgi
      router_xmldir
      rpc
      signal
      spooler
      sqlite3
      sslrouter
      stats_pusher_file
      stats_pusher_socket
      symcall
      syslog
      transformation_chunked
      transformation_gzip
      transformation_offload
      transformation_tofile
      transformation_toupper
      ugreen
      webdav
      zergpool
    ]

    (libexec/"uwsgi").mkpath
    plugins.each do |plugin|
      system "python", "uwsgiconfig.py", "--verbose", "--plugin", "plugins/#{plugin}", "brew"
    end

    python_versions = ["python", "python2"]
    python_versions << "python3" if build.with? "python3"
    python_versions.each do |v|
      system v, "uwsgiconfig.py", "--verbose", "--plugin", "plugins/python", "brew", v
    end

    bin.install "uwsgi"
  end

  plist_options :manual => "uwsgi"

  def plist; <<-EOS.undent
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
      <dict>
        <key>Label</key>
        <string>#{plist_name}</string>
        <key>RunAtLoad</key>
        <true/>
        <key>KeepAlive</key>
        <true/>
        <key>ProgramArguments</key>
        <array>
            <string>#{opt_bin}/uwsgi</string>
            <string>--ini</string>
            <string>#{HOMEBREW_PREFIX}/etc/uwsgi/uwsgi.ini</string>
        </array>
        <key>WorkingDirectory</key>
        <string>#{HOMEBREW_PREFIX}</string>
      </dict>
    </plist>
    EOS
  end

  test do
    (testpath/"helloworld.py").write <<-EOS.undent
      def application(env, start_response):
        start_response('200 OK', [('Content-Type','text/html')])
        return [b"Hello World"]
    EOS

    pid = fork do
      exec "#{bin}/uwsgi --http-socket 127.0.0.1:8080 --protocol=http --plugin python -w helloworld"
    end
    sleep 2

    begin
      assert_match "Hello World", shell_output("curl localhost:8080")
    ensure
      Process.kill("SIGINT", pid)
      Process.wait(pid)
    end
  end
end
